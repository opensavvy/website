# GitLab CI Templates (Deprecated)

GitLab CI YAML templates to use Gradle, Docker and other technologies.

<div class="grid cards" markdown>

- [Repository](https://gitlab.com/opensavvy/ci-templates)
- Licensed under **Apache 2.0**
- This project is [**deprecated**](../open-source/stability.md)

</div>
